" Language:     Colorful CSS Color Preview
" Author:       Aristotle Pagaltzis <pagaltzis@gmx.de>
" Commit:       $Format:%H$
" Licence:      The MIT License (MIT)

if ! ( v:version >= 700 && has('syntax') && has('vim9script') && ( has('gui_running') || &t_Co == 256 ) )
    function css_color#Init(type, keywords, groups)
    endfunction
    function css_color#Extend(groups)
    endfunction
    finish
endif

vim9script

def Rgb2color(r: string, g: string, b: string): string
    # Convert 80% -> 204, 100% -> 255, etc.
    var ToNumber = (idx: number, val: string): number => {
        var res = str2nr(val)
        if val =~ "%$"
            res = ( 255 * res ) / 100
        endif
        return res
    }
    var rgb = map( [r, g, b], ToNumber )
    return printf( '%02x%02x%02x', rgb[0], rgb[1], rgb[2] )
enddef


def Hsl2color(h: string, s: string, l: string): string
    # Convert 80% -> 0.8, 100% -> 1.0, etc.
    var ToFloat = (idx: number, val: string): float => {
        var res = str2float(val)
        if val =~ "%$"
            res /= 100.0
        endif
        return res
    }
    var [ss, ll] = map( [s, l], ToFloat)
    # algorithm transcoded to vim from http://www.w3.org/TR/css3-color/#hsl-color
    var hh = ( str2nr(h) % 360 ) / 360.0
    var m2 = ll <= 0.5 ? ll * ( ss + 1 ) : ll + ss - ll * ss
    var m1 = ll * 2 - m2
    var rgb = []
    for h_ in [ hh + (1 / 3.0), hh, hh - (1 / 3.0) ]
        var hh_ = h_ < 0 ? h_ + 1 : h_ > 1 ? h_ - 1 : h_
        var v =
            \ hh_ * 6 < 1 ? m1 + ( m2 - m1 ) * hh_ * 6 :
            \ hh_ * 2 < 1 ? m2 :
            \ hh_ * 3 < 2 ? m1 + ( m2 - m1 ) * ( 2 / 3.0 - hh_ ) * 6 :
            \ m1
        if v > 1.0 | return '' | endif
        rgb += [ float2nr( 255 * v ) ]
    endfor
    return printf( '%02x%02x%02x', rgb[0], rgb[1], rgb[2] )
enddef

var _1_3 = 1.0 / 3
var _16_116 = 16.0 / 116.0
var cos16 = cos(16 * (180 / atan2(0, -1)))
var sin16 = sin(16 * (180 / atan2(0, -1)))

def Rgb2din99(rgb: list<float>): list<float>
    var Convert = (idx: number, val: float): float => {
        if  val > 0.04045
            return pow((val + 0.055) / 1.055, 2.4)
        else
            return val / 12.92
        endif
    }
    var [r, g, b] = map( copy(rgb), Convert )

    var x = r * 0.4124 + g * 0.3576 + b * 0.1805
    var y = r * 0.2126 + g * 0.7152 + b * 0.0722
    var z = r * 0.0193 + g * 0.1192 + b * 0.9505

    # Observer 2°, Illuminant D65
    x = ( x * 100 ) /  95.0489
    z = ( z * 100 ) / 108.8840

    var Convert_2 = (idx: number, val: float): float => {
        if val > 0.008856
            return pow(val, _1_3)
        else
            return 7.787 * val + _16_116
        endif
    }
    [x, y, z] = map( [x, y, z], Convert_2 )

    var [L, a, bb] = [ (116 * y) - 16, 500 * (x - y), 200 * (y - z) ]

    var L99 = 105.51 * log(1 + 0.0158 * L)

    var e =         a * cos16 + bb * sin16
    var f = 0.7 * (bb * cos16 - a  * sin16)

    g = 0.045 * sqrt(e * e + f * f)
    var a99: float
    var b99: float
    if g == 0
        [a99, b99] = [0.0, 0.0]
    else
        var k = log(1 + g) / g
        a99 = k * e
        b99 = k * f
    endif

    return [L99, a99, b99]
enddef

var hex: dict<any>
for i in range(0, 255)
    hex[ printf( '%02x', i ) ] = i
endfor

var exe = []
def Flush_exe()
    if len(exe) > 0
        exe join( remove( exe, 0, -1 ), ' | ' )
    endif
enddef

if has('gui_running')
    def Create_highlight(color: string, is_bright: bool): bool
        add( exe,
            'hi BG' .. color
            .. ' guibg=#'
            .. color .. ' guifg=#'
            .. ( is_bright ? '000000' : 'ffffff' ) )
        return true
    enddef
else
    # the 16 vt100 colors are not defined consistently
    var xtermcolor = repeat( [''], 16 )

    # the 6 values used in the xterm color cube
    #                    0    95   135   175   215   255
    var cubergb = [ 0x00, 0x5F, 0x87, 0xAF, 0xD7, 0xFF ]
    for rrr in cubergb
        for ggg in cubergb
            for bbb in cubergb
                add( xtermcolor, [ rrr, ggg, bbb ] )
            endfor
        endfor
    endfor

    # grayscale ramp
    xtermcolor += map( range(24), (idx: number, val: number): number => repeat( [10 * val + 8], 3 ) )

    for idx in range( 16, len(xtermcolor) - 1 )
        xtermcolor[idx] = rgb2din99( map(xtermcolor[idx], (idx: number, val: number): float => val / 255.0) )
    endfor

    # selects the nearest xterm color for a rgb value like #FF0000
    def Rgb2xterm(color: string): number
        var best_match = 0
        var smallest_distance = 10000000000
        var color = tolower(color)
        var r = hex[color[0:1]]
        var g = hex[color[2:3]]
        var b = hex[color[4:5]]

        var [L1, a1, b1] = rgb2din99([ r / 255.0, g / 255.0, b / 255.0 ])

        for idx in range( 16, len(xtermcolor) - 1 )
            var [L2, a2, b2] = xtermcolor[idx]
            var dL = L1 - L2
            var da = a1 - a2
            var db = b1 - b2
            var distance = dL*dL + da*da + db*db
            if distance == 0 | return idx | endif
            if distance > smallest_distance | continue | endif
            var smallest_distance = distance
            var best_match = idx
        endfor
        return best_match
    enddef

    var color_idx = {}
    def Create_highlight(color: string, is_bright: bool): bool
        var color_idx = get( color_idx, color, -1 )
        if color_idx == -1
            var color_idx = rgb2xterm(color)
            var color_idx[color] = color_idx
        endif
        add(exe,
            'hi BG' .. color
            .. ' guibg=#' .. color .. ' guifg=#' .. ( is_bright ? '000000' : 'ffffff' )
            .. ' ctermbg=' .. color_idx .. ' ctermfg=' .. ( is_bright ? 0 : 15 )
        )
            return true
        endif
    enddef
endif

def Recreate_highlights()
    filter( copy( b:css_color_hi ), Create_highlight )
enddef

var pattern_color = {}
var color_bright  = {}
def Create_syn_match()

    var pattern = submatch(0)

    if has_key( b:css_color_syn, pattern ) | return | endif
    b:css_color_syn[pattern] = 1

    var rgb_color = get( pattern_color, pattern, '' )

    if  strlen( rgb_color ) == 0
        var hex_ = submatch(1)
        var funcname = submatch(2)

        rgb_color
            \ = funcname == 'rgb' ? Rgb2color(submatch(3), submatch(4), submatch(5))
            \ : funcname == 'hsl' ? Hsl2color(submatch(3), submatch(4), submatch(5))
            \ : strlen(hex_) >= 6  ? tolower(hex_[0 : 5])
            \ : strlen(hex_) >= 3  ? tolower(hex_[0] .. hex_[0] .. hex_[1] .. hex_[1] .. hex_[2] .. hex_[2])
            \ : ''

        if rgb_color == '' | throw 'css_color: create_syn_match invoked on bad match data' | endif
        pattern_color[pattern] = rgb_color
    endif

    if ! has_key( b:css_color_hi, rgb_color )
        var is_bright = get( color_bright, rgb_color, false )
        if is_bright == false
            var r = hex[rgb_color[0 : 1]]
            var g = hex[rgb_color[2 : 3]]
            var b = hex[rgb_color[4 : 5]]
            is_bright = r * 30 + g * 59 + b * 11 > 12000
            color_bright[rgb_color] = is_bright
        endif

        Create_highlight( rgb_color, is_bright )
        b:css_color_hi[rgb_color] = is_bright
    endif

    # iff pattern ends on word character, require word break to match
    if pattern =~ '\>$' | pattern ..= '\>' | endif
    add( exe, 'syn match BG' .. rgb_color .. ' /' .. escape(pattern, '/') .. '/ contained containedin=@colorableGroup' )

    return
enddef

#var css_color_match_id = []
export def Clear_matches()
	map(get(w:, 'css_color_match_id', []), 'matchdelete(v:val)')
    w:css_color_match_id = []
enddef

export def Create_matches()
    Clear_matches()
    if ! &l:cursorline | return | endif
    # adds matches based that duplicate the highlighted colors on the current line
    var lnr = line('.')
    var group = ''
    var groupstart = 0
    var endcol = &l:synmaxcol > 0 ? &l:synmaxcol : col('$')
    for col in range( 1, endcol )
        var nextgroup = col < endcol ? synIDattr( synID( lnr, col, 1 ), 'name' ) : ''
        if group == nextgroup | continue | endif
        if group =~ '^BG\x\{6}$'
            var regex = '\%' .. lnr .. 'l\%' .. groupstart .. 'c' .. repeat( '.', col - groupstart )
            w:css_color_match_id += [ matchadd( group, regex, -1 ) ]
        endif
        group = nextgroup
        groupstart = col
    endfor
enddef

var _hexcolor   = '#\(\x\{3}\%(\>\|\x\{3}\>\)\)' # submatch 1
var _rgbacolor  = '#\(\x\{3}\%(\>\|\x\%(\>\|\x\{2}\%(\>\|\x\{2}\>\)\)\)\)' # submatch 1
var _funcname   = '\(rgb\|hsl\)a\?' # submatch 2
var _ws_        = '\s*'
var _numval     = _ws_ .. '\(\d\{1,3}%\?\)' # submatch 3,4,5
var _listsep    = _ws_ .. ','
var _otherargs_ = '\%(,[^)]*\)\?'
var _funcexpr   = _funcname .. '[(]' .. _numval .. _listsep .. _numval .. _listsep .. _numval .. _ws_ .. _otherargs_ .. '[)]'
var _csscolor   = _rgbacolor .. '\|' .. _funcexpr
# N.B. sloppy heuristic constants for performance reasons:
#      a) start somewhere left of screen in case of partially visible colorref
#      b) take some multiple of &columns to handle multibyte chars etc
# N.B. these substitute() calls are here just for the side effect
#      of invoking create_syn_match during substitution -- because
#      match() and friends do not allow finding all matches in a single
#      scan without examining the start of the string over and over
export def Parse_screen()
    var leftcol = winsaveview().leftcol
    var left_ = max([ leftcol - 15, 0 ])
    var width = &columns * 4

    var r = range( line('w0'), line('w$') )
    var F = (id: number, val: number) => strlen(
        substitute(
            strpart(
                getline(val),
                col([val, left_]),
                width ),
            b:css_color_pat,
            '\=Create_syn_match()',
            'g' ) ) > 0
    filter(
        r,
        F )
    Flush_exe()
enddef

################################################################################

export def Css_color_reinit()
    Recreate_highlights()
    Flush_exe()
enddef

def Css_color_enable()
    if ! b:css_color_off | return | endif
    if len( b:css_color_grp ) > 0
        exe 'syn cluster colorableGroup add=' .. join( b:css_color_grp, ',' )
    endif
    augroup CSSColor
        autocmd! * <buffer>
        if has('nvim-0.3.1')
            autocmd CursorMoved,CursorMovedI <buffer> Parse_screen()
        else
            autocmd CursorMoved,CursorMovedI <buffer> {
                Parse_screen()
                Create_matches()
            }
            autocmd BufWinEnter <buffer> Create_matches()
            autocmd BufWinLeave <buffer> Clear_matches()
        endif
        autocmd ColorScheme <buffer> Css_color_reinit()
    augroup END
    b:css_color_off = false
    doautocmd CSSColor CursorMoved
enddef

def Css_color_disable()
    if b:css_color_off | return | endif
    if len( b:css_color_grp )
        exe 'syn cluster colorableGroup remove=' .. join( b:css_color_grp, ',' )
    endif
    autocmd! CSSColor * <buffer>
    b:css_color_off = true
enddef

export def Toggle()
    if b:css_color_off | Css_color_enable()
    else               | Css_color_disable()
    endif
enddef

var types         = [ 'none', 'hex', 'rgba', 'css', 'none' ] # with wraparound for index() == -1
var pat_for_type = [ '^$', _hexcolor, _rgbacolor, _csscolor, '^$' ]

export def Init(type: string, keywords: string, groups: string)
    var new_type = index( types, type )
    var old_type = index( pat_for_type, get( b:, 'css_color_pat', '$^' ) )


    b:css_color_pat = pat_for_type[ max( [ old_type, new_type ] ) ]
    b:css_color_grp = extend( get( b:, 'css_color_grp', [] ), split( groups, ',' ), 0 )
    b:css_color_hi  = {}
    b:css_color_syn = {}
    b:css_color_off = true

    Css_color_enable()

    if keywords != 'none'
        exe 'syntax include syntax/colornames/' .. keywords .. '.vim'
        extend( color_bright, b:css_color_hi )
    endif
enddef

# utility def for development use
def Css_color_dump_highlights(): string
    Recreate_highlights()
    var cmd = join( sort( remove( exe, 0, -1 ) ), "\n" )
    cmd = substitute( cmd, '#......', '\U&', 'g' )
    cmd = substitute( cmd, "ctermbg=\\zs\\d\\+", "\\=printf(\"%-3d\", submatch(0))", 'g' )
    return cmd
enddef
defcompile
