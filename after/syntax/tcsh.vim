syn match tcshCommentColor contained '\(#[^#]*\)\@<=\zs#\x\{3}\%(\x\{3}\)\?\>' containedin=tcshComment
call css_color#Init( 'hex', 'none'
	\, 'tcshSQuote,tcshDQuote,tcshHereDoc,'
	\. 'tcshCommentColor' )
