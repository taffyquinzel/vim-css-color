" https://github.com/Fymyte/rasi.vim
call css_color#Init('css', 'extended'
    \,  'rasiHexColor,rasiRGBColor,rasiRGBAColor,rasiHSLColor,rasiHSLAColor,rasiNamedColor'
    \. ',rasiPropertyVal,rasiComment,rasiCommentL,rasiEnvVar,rasiVarReference')
