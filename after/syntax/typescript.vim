" https://github.com/HerringtonDarkholme/yats.vim (stock Vim syntax)
" https://github.com/leafgarland/typescript-vim
call css_color#Init('css', 'extended'
 \, 'typescriptString,typescriptStringProperty,typescriptStringS,typescriptStringD,typescriptStringB,'
 \. 'typescriptComment,typescriptLineComment,typescriptCommentSkip,typescriptDocComment'
 \)
